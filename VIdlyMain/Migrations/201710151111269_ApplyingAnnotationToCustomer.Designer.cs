// <auto-generated />
namespace VIdlyMain.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ApplyingAnnotationToCustomer : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ApplyingAnnotationToCustomer));
        
        string IMigrationMetadata.Id
        {
            get { return "201710151111269_ApplyingAnnotationToCustomer"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
