namespace VIdlyMain.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class seedGenre : DbMigration
    {
        public override void Up()
        {
            Sql("insert into Genres values('Action')");
            Sql("insert into Genres values('Thriller')");
            Sql("insert into Genres values('Family')");
            Sql("insert into Genres values('Romance')");
            Sql("insert into Genres values('Comedy')");
        }
        
        public override void Down()
        {
        }
    }
}
