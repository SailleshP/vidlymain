namespace VIdlyMain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedUsers : DbMigration
    {
        public override void Up()
        {

Sql(@"INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6b4f2f97-3e45-4346-a678-e2d551333406', N'admin@gmail.com', 0, N'ACR5LWkvrJs3BMbdx1WPcrSTf/kDyDjUE88/aRKkmfXji2INRWVGH6lSuv97+virQQ==', N'931032d3-bee7-4c73-be94-673e9ed501d8', NULL, 0, 0, NULL, 1, 0, N'admin@gmail.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e33d8312-ebb6-4370-9ae8-27fdc4a746e0', N'guest@gmail.com', 0, N'AKnbPO6+qf6UAL0iT4+xxiHv4Z7qEFJQzkV7fl3RPLjOKsCXTkv1HMG0xa7BcFZUzw==', N'409632ab-399f-4f26-91ba-69113b4ea60c', NULL, 0, 0, NULL, 1, 0, N'guest@gmail.com')
INSERT INTO[dbo].[AspNetRoles]([Id], [Name]) VALUES(N'747402e5-2d26-4688-a698-4fbdf87a0730', N'CanManageMovies')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6b4f2f97-3e45-4346-a678-e2d551333406', N'747402e5-2d26-4688-a698-4fbdf87a0730')

");



        }

    public override void Down()
        {
        }
    }
}

