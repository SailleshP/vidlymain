namespace VIdlyMain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedDOBCustomers : DbMigration
    {
        public override void Up()
        {

            Sql("update Customers set BirthDate=convert(date,'26/11/1989',104) where id='1'");
            Sql("update Customers set BirthDate=convert(date,'20-10-1979',104)  where id='2'");
            Sql("update Customers set BirthDate=convert(date,'12-05-1986',104)   where id='3'");
        }
        
        public override void Down()
        {
        }
    }
}
