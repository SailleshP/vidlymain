﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VIdlyMain.Startup))]
namespace VIdlyMain
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
