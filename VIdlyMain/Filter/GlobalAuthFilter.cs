﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;


namespace VIdlyMain.Filter
{
    public class GlobalAuthFilter: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
        
            if(string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["email"])))
            {

               if(filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 403;
                    filterContext.HttpContext.Response.End();
                    filterContext.Result = new JsonResult { Data = "TimOut" };
                }
               else
                {
                    
                    filterContext.Result=new ViewResult{ ViewName="/Views/Account/Login.cshtml"};
                    filterContext.RouteData.Values["controller"] = "Account";
                    return;
                }

            }

            


        }

    }
}