﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VIdlyMain.Filter;

namespace VIdlyMain.Controllers
{
    [GlobalAuthFilter]
    public class RentalController : Controller
    {
        // GET: Rental
        public ActionResult Index()
        {
            Session["email"] = null;
            return View();
        }
    }
}