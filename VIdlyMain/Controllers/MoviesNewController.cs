﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VIdlyMain.Filter;
using VIdlyMain.Models;
using VIdlyMain.ViewModel;

namespace VIdlyMain.Controllers
{

    [GlobalAuthFilter]
    public class MoviesNewController : Controller
    {
        // GET: Movies
        public ViewResult Random()
        {
            var movie = new Movie() { Name = "Shrek!" };
            ViewData["Movie"] = movie;
            ViewBag.Moview = movie;

            var customers = new List<Customer>
            {
                new Customer()
                {
                    Name="Customer1"
                },
                new Customer()
                {
                    Name="Customer2"
                }

            };


            var viewModel = new RandomMovieViewModel()
            {
                Customers = customers,
                Movie = movie

            };


            return View(viewModel);


            //var viewResult = new ViewResult();
            //viewResult.ViewData.Model = movie;



            //return View(viewResult.Model);

            //return new EmptyResult();

            //return View(movie);

            //return RedirectToAction("index", "home", new { page = 1, sortby = "Name" });
        }


        public ActionResult Edit(int? movieId)
        {
            return Content($"id ={movieId}");
        }

        [Route("movies/released/{year}/{month:regex(\\d{4}):rang(1,12)}")]
        public ActionResult ByReleaseDate(int year,int month)
        {
            return Content($"{year}/{month}");
        }



    }
}