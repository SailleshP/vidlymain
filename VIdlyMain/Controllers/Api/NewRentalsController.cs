﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VIdlyMain.DTO;
using VIdlyMain.Filter;
using VIdlyMain.Models;

namespace VIdlyMain.Controllers.Api
{

    [GlobalAuthFilter]
    public class NewRentalsController : ApiController
    {
        private ApplicationDbContext _context;
        public NewRentalsController()
        {
            _context = new ApplicationDbContext();
        }

        public IHttpActionResult Post(RentalDTO rentalDTO)
        {
            var customer = _context.Customers.SingleOrDefault(cust => cust.Id == rentalDTO.CustomerId);
            if (customer == null) return BadRequest("Invalid Request");
            var movies = _context.Movies.Where(m => rentalDTO.MovieIds.Contains(m.Id)).ToList();

            foreach (var movie in movies)
            {
                if (movie.NumberAvailable == 0) return BadRequest("Movie not available");
                movie.NumberAvailable--;

                var rental = new Rental()
                {
                    Customer=customer,
                    Movie= movie,
                    DateRented=DateTime.Now
                };

                _context.Rental.Add(rental);
            }
            
            _context.SaveChanges();
            return Ok();
            //return Created(new Uri(Request.RequestUri + "/" + customer.Id), customer); multiple resources

        }
    }
}
