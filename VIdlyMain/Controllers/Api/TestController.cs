﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ValueProviders.Providers;
using VIdlyMain.DTO;
using VIdlyMain.Models;

namespace VIdlyMain.Controllers.Api
{
    public class TestController : ApiController
    {
        private ApplicationDbContext _context;
        public TestController()
        {
            _context = new ApplicationDbContext();
        }

        public IHttpActionResult GetMovieQueryString(QueryStringValueProvider values)
        {


            var movie = _context.Movies.SingleOrDefault(c => c.Id == Convert.ToInt32(values.GetValue("id")));

            if (movie == null)
                return NotFound();

            return Ok(Mapper.Map<Movie, MovieDto>(movie));

        }

    }
}
