﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VIdlyMain.DTO;
using VIdlyMain.Filter;
using VIdlyMain.Models;

namespace VIdlyMain.Controllers.Api
{
    [GlobalAuthFilter]
    public class CustomerController : ApiController
    {

        private ApplicationDbContext _context;
        public CustomerController()
        {
            _context = new ApplicationDbContext();

        }
        public IHttpActionResult GetCustomer(string query)
        {
            var customersQuery = _context.Customers
                            .Include(cust => cust.MembershipType);


            if (!string.IsNullOrEmpty(query))
            {
                customersQuery.Where(c => c.Name.Contains(query));

            }
                            //.ToList()
                 var customersDTO= customersQuery
                                    .ToList()
                                    .Select(Mapper.Map<Customer, CustomerDTO>);


            return Ok(customersDTO);

        }


        public IHttpActionResult GetCustomer(int id)
        {
            var customer = _context.Customers.SingleOrDefault(cust => cust.Id == id);
            if (customer == null)
            {

                return NotFound();
            }
            else
            {
                return Ok(Mapper.Map<Customer, CustomerDTO>(customer));

            }


        }



        public IHttpActionResult PostCustomer(CustomerDTO customerDTO)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var customer = Mapper.Map<CustomerDTO, Customer>(customerDTO);
            _context.Customers.Add(customer);
            _context.SaveChanges();

            //api /{ controller}/{ id}
            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customer);
            //  return CreatedAtRoute("DefaultApi", new { id = customer.Id }, customer);
        }

        public IHttpActionResult PutCustomer(int id, CustomerDTO customerDTO)
        {

            if (!ModelState.IsValid) return BadRequest(ModelState);
            var customer = Mapper.Map<CustomerDTO, Customer>(customerDTO);
            if (id != customer.Id)
            {
                return BadRequest();
            }
            _context.Entry(customer).State = EntityState.Modified;
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return Ok();
        }


        public IHttpActionResult Delete(int id)
        {
            Customer customer = _context.Customers.Find(id);
            //if customer object is null
            if (customer == null)
            {
                //return HttpStatusCode.NotFound response
                Request.CreateResponse(HttpStatusCode.NotFound, "No records found");
            }
            //remove from the table
            _context.Customers.Remove(customer);
            //save changes in database
            _context.SaveChanges();
            //return response 200
            return Ok();

        }
    }
}
