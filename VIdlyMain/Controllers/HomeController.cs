﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VIdlyMain.Filter;

namespace VIdlyMain.Controllers
{

    [GlobalAuthFilter]
    public class HomeController : Controller
    {

        //[OutputCache(Duration =60)]

            //[RequireHttps]
        public ActionResult Index()
        {
            //  throw new Exception();
          
           // var method = this.HttpContext.Request.RequestType;
            return View();
        }

        public ActionResult About()
        {

            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}