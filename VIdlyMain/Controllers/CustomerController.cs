﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VIdlyMain.Models;
using System.Data.Entity;
using VIdlyMain.ViewModel;
using VIdlyMain.Filter;

namespace VIdlyMain.Controllers
{

    [GlobalAuthFilter]
    public class CustomerController : Controller
    {

        private ApplicationDbContext _context;

        public CustomerController()
        {
            _context = new ApplicationDbContext();  
        }


        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        private List<Customer> GetCustomer()
        {


             var customers = new List<Customer>
            {
                new Customer()
                {
                    Id=1,
                    Name="Customer1"
                },
                new Customer()
                {
                    Id=2,
                    Name="Customer2"
                }

            };

            return customers;
        }




        // GET: Cusotmer
        public ActionResult Index()
        {
            //var customers = _context.Customers.Include(c=>c.MembershipType).ToList();

            //return View(customers);
            return View();
        }

        // GET: Cusotmer/Details/5
        public ActionResult Details(int id)
        {
            var customer = _context.Customers.SingleOrDefault(cust => cust.Id == id);
            if (customer == null) return HttpNotFound();
            return View(customer);
        }

      

        // POST: Cusotmer/Create
        [HttpPost]
        public ActionResult Save(Customer Customer)
        {
            if (ModelState.IsValid)
            {
                if(Customer.Id==0)
                {
                    _context.Customers.Add(Customer);
                }
                else
                {

                    var customerInDb = _context.Customers.SingleOrDefault(cust => cust.Id == Customer.Id);
                    customerInDb.Name = Customer.Name;
                    customerInDb.IsSubcribedToNewsLetter = Customer.IsSubcribedToNewsLetter;
                    customerInDb.MembershipTypeId = Customer.MembershipTypeId;
                    customerInDb.BirthDate = Customer.BirthDate;
                    //TryUpdateModel(customerInDb); prone to attack
                }


                
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            // TODO: Add insert logic here
            else
            {
                var viewModel = new CustomerViewModel
                {
                    Customer = Customer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };
               
                return View("CustomerForm", viewModel);
            }

               
           
        }

        // GET: Cusotmer/Edit/5
        public ActionResult Edit(int id)
        {

            var customer= _context.Customers.FirstOrDefault(cust => cust.Id == id);

            if (customer == null) return HttpNotFound();
            var CustomerViewModel = new  CustomerViewModel()
            {
                Customer = customer,
                 MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm", CustomerViewModel);
        }

        // POST: Cusotmer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cusotmer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Cusotmer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult New()
        {
            var membershipTypes = _context.MembershipTypes.ToList();
            var customer = new Customer();
            var customerViewModel = new CustomerViewModel()
            {
                MembershipTypes = membershipTypes,
                Customer = new Customer()
            };
            return View("CustomerForm",customerViewModel);
        }


    }
}
