﻿$(document).ajaxComplete(function myErrorHandler(event, xhr, ajaxOptions, thrownError) {
    alert("Ajax request completed with response code " + xhr.status);
    if (xhr.status === 0) {
        alert("You are not connected to the Internet or your session has been expired");
        window.location = "/Account/Login";
    }
    else if (xhr.status === 403) {
        alert("You session has been expired");
        window.location = "/Account/Login";
    }
    else if (xhr.status === 400) {
        alert("Requested Page no found [404]", null, true);
        window.location = "/Account/Login";
    }
    else if (xhr.status === 500) {
        alert("Internal Server Error [404] Please try again later", null, true);
        window.location = "/Account/Login";
    }

});

$(function () {
    $.ajaxSetup({
        error: function (x, exception, error) {
            debugger;
            if (x.status === 0) {
                alert("You are not connected to the Internet or your session has been expired");
                window.location = "/Login/Index";
            }
            else if (x.status === 403) {
                alert("You session has been expired");
                window.location = "/Login/Index";
            }
            else if (x.status === 400) {
                alert("Requested Page no found [404]", null, true);
                window.location = "/Login/Index";
            }
            else if (x.status === 500) {
                alert("Internal Server Error [404] Please try again later", null, true);
                window.location = "/Login/Index";
            }
        }


    });




});

