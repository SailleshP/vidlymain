﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIdlyMain.Models;

namespace VIdlyMain.ViewModel
{
    public class MovieViewModel
    {
        public Movie Movie { get; set; }

        public IList<Genre> Genre { get; set; }
    }
}