﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIdlyMain.Models;

namespace VIdlyMain.ViewModel
{
    public class CustomerViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }

        public Customer Customer { get; set; }

    }
}