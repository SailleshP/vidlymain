﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VIdlyMain.Models;

namespace VIdlyMain.DTO
{
    public class CustomerDTO
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? BirthDate { get; set; }
        public bool IsSubcribedToNewsLetter { get; set; }
        public MemberShipTypeDTO MembershipType { get; set; }

        [Required]
        public byte MembershipTypeId { get; set; }
    }
}