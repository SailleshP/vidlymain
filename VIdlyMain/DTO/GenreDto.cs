﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VIdlyMain.DTO
{
    public class GenreDto
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}