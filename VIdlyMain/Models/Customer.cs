﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VIdlyMain.Models
{
    public class Customer
    {
        public int Id{ get; set; }

        [Required]
        [StringLength(255)]
        public string Name{ get; set; }


        [Display(Name="Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Min18YearIfMember]
        public DateTime ?BirthDate { get; set; }
        public bool IsSubcribedToNewsLetter { get; set; }

        public MembershipType MembershipType { get; set; }

        [Required]
        [Display(Name="Membership Type")]
        public byte MembershipTypeId { get; set; }


    }
}