﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VIdlyMain.Models
{
    public class Movie
    {
        public int Id{ get; set; }


        [Required]
        public string Name{ get; set; }

        [Required]
        [Display(Name = "Released Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }

        public Genre Genre { get; set; }
        [Display(Name = "Genre Type")]



        [Required]
        public int GenreId { get; set; }

        [Required]
        [Display(Name = "Number In Stock")]
        [Range(1,100)]
        public int NumberInStock{ get; set; }

        public byte NumberAvailable { get; set; }
    }
}
