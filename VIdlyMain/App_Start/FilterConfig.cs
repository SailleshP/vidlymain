﻿using System.Web;
using System.Web.Mvc;
using VIdlyMain.Filter;

namespace VIdlyMain
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthorizeAttribute());
            //filters.Add(new GlobalAuthFilter());
           // filters.Add(new RequireHttpsAttribute());
        }
    }
}
