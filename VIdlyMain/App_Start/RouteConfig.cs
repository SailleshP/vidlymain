﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace VIdlyMain
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

           // routes.MapMvcAttributeRoutes();
           // routes.MapRoute(
           //     "MovieByReleasedDate",
           //     "{movies}/{released}/{year}/{month}",
           //    new { controller = "movies", action = "ByReleaseDate", id = UrlParameter.Optional },
           //    new { year = @"\d{4}", month = @"\d{2}" }
           //);


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                //constraints:new {id=@"\d+"}
            );
        }
    }
}
