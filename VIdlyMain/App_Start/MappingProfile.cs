﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VIdlyMain.DTO;
using VIdlyMain.Models;

namespace VIdlyMain.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerDTO>().ReverseMap().ForMember(c=>c.Id,opt=>opt.Ignore());
            CreateMap<MembershipType, MemberShipTypeDTO>().ReverseMap().ForMember(c => c.Id, opt => opt.Ignore());
            CreateMap<Movie, MovieDto>().ReverseMap().ForMember(c => c.Id, opt => opt.Ignore());
            CreateMap<Genre, GenreDto>().ReverseMap().ForMember(c => c.Id, opt => opt.Ignore());

            CreateMap<Rental, RentalDTO>().ReverseMap().ForMember(c => c.Id, opt => opt.Ignore());

        }


    }
}